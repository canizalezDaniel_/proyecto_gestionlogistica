﻿@ModelType LoginViewModel


<div class="row">
     <br />
    <div class="col-md-offset-2 col-md-6">
        <section id="loginForm">
            @Using Html.BeginForm("Login", "Account", New With {.ReturnUrl = ViewBag.ReturnUrl}, FormMethod.Post, New With {.class = "form-horizontal", .role = "form"})
                @Html.AntiForgeryToken()
                @<text>
                <h4>Portal</h4>
                <label>Iniciar sesión</label>
                <hr />
                @Html.ValidationSummary(True, "", New With {.class = "text-danger"})
                <div class="form-group">
                   
                    <div class="col-md-10">
                        <label>Nombre de usuario</label>
                        @Html.TextBoxFor(Function(m) m.Email, New With {.class = "form-control"})
                        @Html.ValidationMessageFor(Function(m) m.Email, "", New With {.class = "text-danger"})
                    </div>
                </div>
                <div class="form-group">
                    
                    <div class="col-md-10">
                        <label>Contraseña</label>
                        @Html.PasswordFor(Function(m) m.Password, New With {.class = "form-control"})
                        @Html.ValidationMessageFor(Function(m) m.Password, "", New With {.class = "text-danger"})
                    </div>
                </div>
               
                <div class="form-group">
                    <div class="col-md-10">
                        <input type="submit" value="Iniciar sesión" class="btn btn-default" />
                    </div>
                </div>
                <p>
                   <label>Si no recuerda su contraseña, pulse </label> @Html.ActionLink("aquí", "Register")
                </p>
                @* Enable this once you have account confirmation enabled for password reset functionality
                    <p>
                        @Html.ActionLink("Forgot your password?", "ForgotPassword")
                    </p>*@
                </text>
            End Using
        </section>

       
    </div>
    <br /><br /><br /><br /><br />
    <div class="col-md-4">
        <section id="socialLoginForm">

            <img src="@Url.Content("~/Content/img/logo.png")" alt="Image" width="180" height="180" />

        </section>
    </div>
    
</div>
@Section Scripts
    @Scripts.Render("~/bundles/jqueryval")
End Section
